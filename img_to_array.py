#!/usr/bin/python

import sys

infile=sys.argv[1]
outfile=sys.argv[2]
fout=open(outfile, 'wb')

with open(infile, "rb") as f:
    byte = f.read(1)
    while byte != "":
        newb= "0x"+byte.encode('hex')+","
        fout.write(newb);
        print(newb)
        byte = f.read(1)
    f.close()
fout.close()


print "The arguments are:", str(infile), str(outfile)

